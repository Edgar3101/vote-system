<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\{PollingModel, User};
use Illuminate\Support\Facades\Validator;

class PollingController extends Controller
{   

    protected $polling;
    protected $user;

    public function __construct() {
        $this->polling= new PollingModel();
        $this->user = new User();
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {   
        $data = DB::table($this->polling->table . " AS p")
        ->leftJoin($this->user->table . " AS u", "p.created_by", "=", "u.id")
        ->select("p.name", "p.description", "p.status")
        ->get();

        return view("index", ["data" => $data]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $validation = Validator::make($request->all(), 
        [
            "title" => "required|string",
            "description" => "required|string",
            "status" => "integer"
        ],
        [
            "title.required" => "El título es requerido",
            "title.string" => "El título debe ser una cadena de texto",
            "description.required" => "La descripción es requerida",
            "description.string" => "La descripción debe ser una cadena de texto",
            "status.integer" => "El estado debe ser un número entero",
        ]);

        if(!$validation->fails()){
            return response()->json($validation->errors(), 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(PollingModel $pollingModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PollingModel $pollingModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PollingModel $pollingModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PollingModel $pollingModel)
    {
        //
    }
}
