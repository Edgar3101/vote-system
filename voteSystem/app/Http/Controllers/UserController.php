<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return User::where("role", 0)->get();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $validation = Validator::make($request->all(), 
        [
            "name" => "required|string",
            "email" => "required|email|unique:users",
            "password" => "required|string|min:8",
            "role" => "required|integer"
        ],
        [
            "name.required" => "El campo nombre es requerido",
            "name.string" => "El campo nombre debe ser un string",
            "email.required" => "El campo email es requerido",
            "email.email" => "El campo email debe ser un email",
            "email.unique" => "El email ya existe",
            "password.required" => "El campo contraseña es requerido",
            "password.string" => "El campo contraseña debe ser un string",
            "password.min" => "El campo contraseña debe tener al menos 8 caracteres",
        ]);

        if($validation->fails()){
            return response()->json($validation->errors(), 400);
        }

        return User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => $request->password,
            "role" => $request->role
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}
