<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class QuestionModel extends Model
{
    use HasFactory;
    use HasUuids;

    protected $table = "question";

    protected $fillable = [
        "polling_id",
        "question",
        "type",
        "option"
    ];

    public function polling(): BelongsTo{
        return $this->belongsTo(PollingModel::class, "polling_id");
    }
}
