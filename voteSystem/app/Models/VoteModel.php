<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class VoteModel extends Model
{
    use HasFactory;
    use HasUuids;

    protected $table = "vote";

    protected $fillable = [
        "user_id",
        "polling_id",
        "vote"
    ];

    public function user(): BelongsTo{
        return $this->belongsTo(User::class, "user_id");
    }

    public function polling(): BelongsTo{
        return $this->belongsTo(PollingModel::class, "polling_id");
    }
}
