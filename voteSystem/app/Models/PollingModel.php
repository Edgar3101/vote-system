<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class PollingModel extends Model
{
    use HasFactory;
    use HasUuids;


    public $table = "polling";

    protected $fillable = [
        "name",
        "description",
        "created_by",
        "status"
    ];

    public function user() :BelongsTo {
        return $this->belongsTo(User::class, "created_by");
    }
}
