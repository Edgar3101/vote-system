<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{PollingController, Login};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** 
 * Render the login view
 */
Route::get("/login", function(){
    return view("login");
})->name("login");

/**
 * Render the createUser view
 */
Route::get("/createUser", function(){
    return view("createUser");
})->name("createUser");
