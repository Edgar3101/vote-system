<x-layout>
    <div class="container-fluid" style="height: 100vh; background: rgb(191,34,195); background: linear-gradient(0deg, rgba(191,34,195,1) 0%, rgba(45,108,253,1) 100%);">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%;">
        <div class="card" style="width: 30rem;">
            <div class="card-body">
            <h5 class="card-title text-center">Iniciar Sesion</h5>

            <form>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Correo Electronico</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Contraseña</label>
                  <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
                <p class="text-center"> Si no tienes un usuario puedes crear un usuario <a href="{{ route('createUser') }}">aquí</a> </p>
                <button type="submit" class="btn btn-primary">Login</button>
              </form>
            
        </div>
        </div>
    </div>

</x-layout>