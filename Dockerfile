FROM php:8.2-fpm

WORKDIR /app

#RUN apt update && apt install git
RUN apt update && apt install -y zlib1g-dev libzip-dev libfreetype6-dev libargon2-0-dev libjpeg62-turbo-dev libpng-dev libicu-dev nano && docker-php-ext-install exif gd zip pdo_mysql intl mysqli

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

EXPOSE 8000

CMD []